from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from .models import ConferenceVO
from django.views.decorators.http import require_http_methods
import json
# from .api_views import ConferenceListEncoder, ConferenceDetailEncoder


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]

class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
        "id"
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse({"attendees": attendees}, encoder=AttendeeEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse({"message": "Invalid state abbreviation"}, status=400)
        attendee = Attendee.objects.create(**content)
        return JsonResponse(attendee, encoder=AttendeeEncoder, safe=False)

    



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse({"attendees": attendee}, encoder=AttendeeEncoder, safe=False)
    elif request.method == "DELETE":
        try:
            Attendee.objects.get(id=id).delete()
            return JsonResponse({"Deleted": True})
        except Attendee.DoesNotExist:
            return JsonResponse({"message": "attendee does not exist"}, status=400)
    else:
        content = json.loads(request.body)
        try:
            Attendee.objects.filter(id=id).update(**content)
            attendee = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return JsonResponse({"message": "attendee does not exist"}, status=400)
        return JsonResponse({"attendee": attendee}, encoder=AttendeeEncoder, status=400)

        # try:
        #     if "attendee" in content:
        #         content["attendee"] = Attendee.objects.get(id=id)
        # except Attendee.DoesNotExist:
        #     return JsonResponse({"message": "attendee does not exist"}, status=400)
        # Attendee.objects.filter(id=id)update(**content)
        # attendee = Attendee.objects.get(id=id)
        # return JsonResponse({"attendee": attendee}, encoder=AttendeeEncoder, status=400)

    # return JsonResponse({"attendee": attendee})
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    
