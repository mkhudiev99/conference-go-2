from django.http import JsonResponse
from common.json import ModelEncoder
from .models import State
from .models import Conference, Location
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo


class StateListEncoder(ModelEncoder):
    model = State
    properties = [
        "name",
        "abbreviation",
    ]


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "id",
        "name",
        "city",
        "room_count",
        "updated",
        "created",
        "state",
        "picture_url",
    ]
    encoders = {
        "state": StateListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name", "id"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "id",
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse({"conferences": conferences}, encoder=ConferenceListEncoder)
    else:
        content = json.loads(request.body)
        try:
            content["location"] = Location.objects.get(id=content["location"])
            conference = Conference.objects.create(**content)
            # return JsonResponse({"conference": conference}, encoder=ConferenceDetailEncoder)
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid state abbreviation"}, status=400)
        return JsonResponse({"conference": conference}, encoder=ConferenceDetailEncoder)
        


    
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse({"conference": conference}, encoder=ConferenceDetailEncoder, safe=False)
    elif request.method == "DELETE":
        try:
            Conference.objects.get(id=id).delete()
            return JsonResponse({"message": True})
        except Conference.DoesNotExist:
            return JsonResponse({"message": "conference does not exist"}, status=400)
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "location does not exist"}, status=400)
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse({"conference": conference}, encoder=ConferenceDetailEncoder, safe=False)






    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """



@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            # state = State.objects.get(abbreviation=content["state"])
            # content["state"] = state
            content["state"] = State.objects.get(abbreviation=content["state"])

            photo = get_photo(content["city"], content["state"].abbreviation)
            content.update(photo)

            location = Location.objects.create(**content)
        except State.DoesNotExist:
            return JsonResponse({"message": "Invalid state abbreviation"}, status=400)
        return JsonResponse(location, encoder=LocationListEncoder, safe=False)


    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse({"locations": location}, encoder=LocationListEncoder, safe=False)
    elif request.method == "DELETE":
        try:
            location = Location.objects.get(id=id).delete()
            return JsonResponse({"Deleted": True})
        except Location.DoesNotExist:
            return JsonResponse({"message": "location does not exist"}, status=400)
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse({"message": "state does not exist"}, status=400)
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)


        # location = Location.objects.update(**content)
        # locationns = Location.objects.get(id=id)
        return JsonResponse({"location": location}, encoder=LocationListEncoder, safe=False)

    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """


