from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from events.models import Conference
from django.views.decorators.http import require_http_methods
import json
from events.api_views import ConferenceListEncoder
import pika

# class StatusEncoder(ModelEncoder):
#     model = Status
#     properties = {
#         "name"
#     }

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "id",
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
        # "status": StatusEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse({"presentations": presentations}, encoder=PresentationDetailEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            content["conference"] = Conference.objects.get(id=conference_id)
            # content["status"] = Status.objects.get(name=content["status"])
            # presentation = Presentation.objects.create(**content)
            presentation = Presentation.create(**content)

            # content["presentation"] = Presentation.objects.get(conference_id=content["presentation"])
            # presentations = Presentation.objects.create(**content)
        except Presentation.DoesNotExist:
            return JsonResponse({"message": "conference invalid"}, status=400)
        return JsonResponse(presentation, encoder=PresentationDetailEncoder, safe=False)
            


           
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse({"presentation": presentation}, encoder=PresentationDetailEncoder, safe=False)
    elif request.method == "DELETE":
        try:
            presentation = Presentation.objects.get(id=id).delete()
            return JsonResponse({"Deleted": True})
        except Presentation.DoesNotExist:
            return JsonResponse({"message": "presentation does not exist"}, status=400)
    else:
        content = json.loads(request.body)
        try:
            if "presentation" in content:
                content["presentation"] = Presentation.objects.get(id=id)
        except Conference.DoesNotExist:
            return JsonResponse({"message": "conference does not exist"}, status=400)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse({"conference": presentation}, encoder=PresentationDetailEncoder, safe=False)


# @require_http_methods(["PUT"])
# def api_approve_presentation(request, pk):
#     presentation = Presentation.objects.get(id=pk)
#     presentation.approve()
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )


# @require_http_methods(["PUT"])
# def api_reject_presentation(request, pk):
#     presentation = Presentation.objects.get(id=pk)
#     presentation.reject()
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )
    
@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    send_to_queue(presentation)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    send_to_queue(presentation)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
    
       
def send_to_queue(presentation):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="tasks")

    bodydict = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
        "status": presentation.status.name
    }
    body = json.dumps(bodydict)
    channel.basic_publish(exchange="", routing_key="tasks", body=body)
    connection.close()
